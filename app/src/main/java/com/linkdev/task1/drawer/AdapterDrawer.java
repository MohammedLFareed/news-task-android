package com.linkdev.task1.drawer;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.linkdev.task1.R;
import com.linkdev.task1.common.helpers.Constants;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Mohammed.AbdElAzeem on 12/4/2017.
 */

public class AdapterDrawer extends RecyclerView.Adapter<AdapterDrawer.DrawerViewHolder> {
    private static final String TAG = AdapterDrawer.class.getSimpleName() + Constants.TAG_UNIVERSAL_LOG;
    private final IContractDrawer.IDrawerPresenter mINewsPresenter;

    AdapterDrawer(IContractDrawer.IDrawerPresenter iNewsPresenter) {
        mINewsPresenter = iNewsPresenter;
    }

    @Override
    public DrawerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new DrawerViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_drawer, parent, false));
    }

    @Override
    public void onBindViewHolder(DrawerViewHolder holder, int position) {
        mINewsPresenter.onDrawerBindHolder(holder, position);
    }

    @Override
    public int getItemCount() {
        return mINewsPresenter.getDrawerItemCount();
    }

    //---------- ViewHolder -----------------
    public class DrawerViewHolder extends RecyclerView.ViewHolder implements IDrawerViewHolder {
        private final String TAG = DrawerViewHolder.class.getSimpleName() + Constants.TAG_UNIVERSAL_LOG;

        @BindView(R.id.img_drawer)
        ImageView mDrawerImageView;
        @BindView(R.id.tv_drawer)
        TextView mDrawerTextView;

        View.OnClickListener mItemClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mINewsPresenter.onNavigationItemSelected(getAdapterPosition());
            }
        };

        DrawerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @Override
        public void bind(ModelDrawerItem drawerItem) {
            mDrawerImageView.setImageResource(drawerItem.getIcon());
            mDrawerTextView.setText(drawerItem.getTitleId());

            itemView.setOnClickListener(mItemClickListener);
        }

    }

    //*------- ViewHolderInterface
    public interface IDrawerViewHolder {
        void bind(ModelDrawerItem drawerItem);

    }
}
