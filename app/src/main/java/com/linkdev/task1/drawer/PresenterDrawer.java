package com.linkdev.task1.drawer;

import com.linkdev.task1.R;

/**
 * Created by Mohammed.AbdElAzeem on 12/10/2017.
 */

public class PresenterDrawer implements IContractDrawer.IDrawerPresenter {
    private IContractDrawer.IDrawerView mIDrawerView;

    private int[] mDrawerTitlesIds = {R.string.news, R.string.innovation_map, (R.string.events_calender),
            (R.string.leadership_thoughts), (R.string.language), (R.string.logout)};

    private int[] mDrawerIcons = {R.drawable.news_icon, R.drawable.map_icon, R.drawable.events_icon,
            R.drawable.leadership_icon, R.drawable.language, R.drawable.logout};

    public PresenterDrawer(IContractDrawer.IDrawerView iDrawerView) {
        this.mIDrawerView = iDrawerView;
    }

    @Override
    public void onDrawerBindHolder(AdapterDrawer.IDrawerViewHolder iHolder, int position) {
        iHolder.bind(new ModelDrawerItem(mDrawerTitlesIds[position], mDrawerIcons[position]));
    }

    @Override
    public int getDrawerItemCount() {
        return mDrawerTitlesIds.length;
    }

    @Override
    public void onNavigationItemSelected(int adapterPosition) {
        mIDrawerView.onNavigationItemSelected(mDrawerTitlesIds[adapterPosition], adapterPosition);
    }
}
