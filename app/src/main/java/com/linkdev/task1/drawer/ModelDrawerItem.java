package com.linkdev.task1.drawer;

/**
 * Created by Mohammed.AbdElAzeem on 12/5/2017.
 */

public class ModelDrawerItem {
    private int titleId;
    private int icon;

    ModelDrawerItem(int drawerTitle, int drawerIcon) {
        this.titleId = drawerTitle;
        this.icon = drawerIcon;
    }

    public int getTitleId() {
        return titleId;
    }

    public void setTitleId(int titleId) {
        this.titleId = titleId;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }
}
