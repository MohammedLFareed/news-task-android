package com.linkdev.task1.drawer;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.login.LoginManager;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.linkdev.task1.R;
import com.linkdev.task1.common.base.BaseActivity;
import com.linkdev.task1.common.helpers.AppPreferences;
import com.linkdev.task1.common.helpers.Constants;
import com.linkdev.task1.common.models.ModelLogin;
import com.linkdev.task1.innovation_maps.FragmentInnovationMap;
import com.linkdev.task1.login.ActivityLogin;
import com.linkdev.task1.news.FragmentNews;
import com.linkdev.task1.splash.ActivitySplash;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.linkdev.task1.common.helpers.Constants.EVENTS_CALENDER;
import static com.linkdev.task1.common.helpers.Constants.INNOVATION_MAP;
import static com.linkdev.task1.common.helpers.Constants.LANGUAGE;
import static com.linkdev.task1.common.helpers.Constants.LEADERSHIP_THOUGHTS;
import static com.linkdev.task1.common.helpers.Constants.LOG_OUT;
import static com.linkdev.task1.common.helpers.Constants.NEWS;

public class ActivityMain extends BaseActivity implements IContractDrawer.IDrawerView {
    private static final String TAG = ActivityMain.class.getSimpleName() + Constants.TAG_UNIVERSAL_LOG;

    @BindView(R.id.rv_drawer)
    RecyclerView mDrawerRecyclerView;

    private IContractDrawer.IDrawerPresenter mIDrawerPresenter;
    private int mSelectedNavigationItem = 0;

    public static void init(Context context) {
        context.startActivity(new Intent(context, ActivityMain.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initializeViews();
        Toolbar toolbar = findViewById(R.id.toolbar);
        setToolbar(toolbar, getString(R.string.news), false, true);
        mIDrawerPresenter = new PresenterDrawer(this);

        setupDrawer(toolbar);
        loadFragment(FragmentNews.newInstance());
    }

    private void loadFragment(Fragment fragment) {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fl_main_container, fragment)
                .commit();
    }

    private void setupDrawer(Toolbar toolbar) {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        mDrawerRecyclerView.setHasFixedSize(true);
        mDrawerRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mDrawerRecyclerView.setAdapter(new AdapterDrawer(mIDrawerPresenter));

//        Header setup
        View headerView = ((NavigationView) findViewById(R.id.nav_view)).getHeaderView(0);
        TextView nameNavHeader = headerView.findViewById(R.id.tv_navheader_name);
        TextView emailNavHeader = headerView.findViewById(R.id.tv_navheader_email);
        ModelLogin loginModel = AppPreferences.readModelLoginSP(this);
        if (loginModel != null) {
            nameNavHeader.setText(loginModel.getName());
            emailNavHeader.setText(loginModel.getEmail());
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void initializeViews() {
        ButterKnife.bind(this);
    }

    @Override
    protected void setListeners() {
    }

    @Override
    public void onNavigationItemSelected(int title, int adapterPosition) {
        // close the drawer
        mSelectedNavigationItem = adapterPosition;
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);

        switch (adapterPosition) {
            case NEWS:
                getSupportActionBar().setTitle(R.string.news);
                loadFragment(FragmentNews.newInstance());
                break;
            case INNOVATION_MAP:
                getSupportActionBar().setTitle(R.string.innovation_map);
                loadFragment(FragmentInnovationMap.newInstance());
                break;
            case EVENTS_CALENDER:
                Toast.makeText(this, title, Toast.LENGTH_SHORT).show();
                break;
            case LEADERSHIP_THOUGHTS:
                Toast.makeText(this, title, Toast.LENGTH_SHORT).show();
                break;
            case LANGUAGE:
                ActivitySplash.restart_ChangeLocale(this, true);
                finish();
                break;
            case LOG_OUT:
                GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                        .requestEmail()
                        .build();
                GoogleApiClient mGoogleApiClient = new GoogleApiClient.Builder(this)
                        .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                        .build();
                if (mGoogleApiClient.isConnected()) {
                    if (Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient) != null) {
                        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                                new ResultCallback<Status>() {
                                    @Override
                                    public void onResult(Status status) {
                                        // ...
                                    }
                                });
                    }
                    Log.d(TAG, "onNavigationItemSelected: Google signout");
                }
                if (AccessToken.getCurrentAccessToken() != null) { //fb logout
                    Log.d(TAG, "onNavigationItemSelected.logout: was fb loggedIn");
                    LoginManager.getInstance().logOut();
                }
                finishLogout();
                break;
        }
    }

    private void finishLogout() {
        AppPreferences.writeModelLoginToSP(this, null);
        ActivityLogin.init(this);
        finish();
    }
}
