package com.linkdev.task1.drawer;

/**
 * Created by Mohammed.AbdElAzeem on 12/10/2017.
 */

public interface IContractDrawer {
    public interface IDrawerView {
        void onNavigationItemSelected(int titleId, int adapterPosition);

    }

    public interface IDrawerPresenter {
        void onDrawerBindHolder(AdapterDrawer.IDrawerViewHolder holder, int position);

        int getDrawerItemCount();

        void onNavigationItemSelected(int adapterPosition);
    }
}
