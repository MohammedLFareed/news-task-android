package com.linkdev.task1.login;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.linkdev.task1.R;
import com.linkdev.task1.common.base.BaseActivity;
import com.linkdev.task1.common.helpers.Constants;
import com.linkdev.task1.drawer.ActivityMain;
import com.twitter.sdk.android.core.DefaultLogger;
import com.twitter.sdk.android.core.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterConfig;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ActivityLogin extends BaseActivity implements IContractLogin.IViewLogin {
    private static final String TAG = ActivityLogin.class.getSimpleName() + Constants.TAG_UNIVERSAL_LOG;
    private static final int RC_SIGN_IN = 1995;
    private CallbackManager mCallbackManager;
    private IContractLogin.IPresenterLogin mIPresenterLogin;
    private TwitterAuthClient mTwitterAuthClient;

    @BindView(R.id.pb_login)
    ProgressBar mProgressbar;


    public static void init(Context context) {
        context.startActivity(new Intent(context, ActivityLogin.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        initializeViews();
        setListeners();

        TwitterConfig config = new TwitterConfig.Builder(this)
                .logger(new DefaultLogger(Log.DEBUG))
                .twitterAuthConfig(new TwitterAuthConfig(getString(R.string.com_twitter_sdk_android_CONSUMER_KEY), getString(R.string.com_twitter_sdk_android_CONSUMER_SECRET)))
                .debug(true)
                .build();
        Twitter.initialize(config);

        // facebook config
        mCallbackManager = CallbackManager.Factory.create();

        mIPresenterLogin = new PresenterLogin(this, this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // facebook login
        mCallbackManager.onActivityResult(requestCode, resultCode, data);
        //twitter login
        if (requestCode == TwitterAuthConfig.DEFAULT_AUTH_REQUEST_CODE)
            mTwitterAuthClient.onActivityResult(requestCode, resultCode, data);
        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            mIPresenterLogin.handleSignInResult(result);
        }
    }

    @Override
    protected void initializeViews() {
        ButterKnife.bind(this);
    }

    @OnClick({R.id.btn_fb_login, R.id.btn_tw_login, R.id.btn_ggle_login})
    void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_fb_login:
                // facebook login
                mIPresenterLogin.onFacebookLogin(this, mCallbackManager);
                break;
            case R.id.btn_tw_login:
                // twitter login
                mTwitterAuthClient = mIPresenterLogin.onTwitterLogin(this);
                break;
            case R.id.btn_ggle_login: // Google login
                mIPresenterLogin.onGoogleLogin();
                break;
        }
    }

    @Override
    protected void setListeners() {
    }

    @Override
    public void isShowProgress(Boolean isShow) {
        if (isShow)
            mProgressbar.setVisibility(View.VISIBLE);
        else
            mProgressbar.setVisibility(View.GONE);
    }

    @Override
    public void onLoginSuccess() {
        ActivityMain.init(this);
        finish();
    }

    @Override
    public void onError(String error) {
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void googleStartActivityForResult(Intent signInIntent, int rcSignIn) {
        startActivityForResult(signInIntent, rcSignIn);
    }
}
