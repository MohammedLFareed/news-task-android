package com.linkdev.task1.login;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.linkdev.task1.R;
import com.linkdev.task1.common.base.BasePresenter;
import com.linkdev.task1.common.helpers.AppPreferences;
import com.linkdev.task1.common.helpers.Constants;
import com.linkdev.task1.common.models.ModelLogin;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterApiClient;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;
import com.twitter.sdk.android.core.models.User;
import com.twitter.sdk.android.core.services.AccountService;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

import retrofit2.Call;

import static com.linkdev.task1.common.helpers.Constants.RC_SIGN_IN;

/**
 * Created by Mohammed.AbdElAzeem on 12/12/2017.
 */

public class PresenterLogin extends BasePresenter implements IContractLogin.IPresenterLogin {

    private static final String TAG = PresenterLogin.class.getSimpleName() + Constants.TAG_UNIVERSAL_LOG;
    private IContractLogin.IViewLogin mIViewLogin;
    private Context mContext;

    PresenterLogin(IContractLogin.IViewLogin iViewLogin, Context mContext) {
        this.mIViewLogin = iViewLogin;
        this.mContext = mContext;
    }

    //    --------- FaceBook START ----------
    private GraphRequest.GraphJSONObjectCallback mCallbackGraphJSONObject = new GraphRequest.GraphJSONObjectCallback() {
        @Override
        public void onCompleted(
                JSONObject object,
                GraphResponse response) {
            try {
                String name = object.getString("name");
                String email = object.getString("email");
                Log.d(TAG, "onCompleted: fb " + name + " " + email);

                finishLogin(name, email);
            } catch (JSONException e) {
                Log.e(TAG, "onCompleted: ", e);
            }
        }
    };
    private FacebookCallback<LoginResult> mCallbackLoginResultFacebook = new FacebookCallback<LoginResult>() {
        @Override
        public void onSuccess(final LoginResult loginResult) {
            mIViewLogin.isShowProgress(false);
            GraphRequest request =
                    GraphRequest.newMeRequest(loginResult.getAccessToken(),
                            mCallbackGraphJSONObject);
            Bundle parameters = new Bundle();
            parameters.putString("fields", "name, email");
            request.setParameters(parameters);
            request.executeAsync();
        }

        @Override
        public void onCancel() {
            mIViewLogin.isShowProgress(false);
            mIViewLogin.onError(mContext.getString(R.string.login_canceled));
        }

        @Override
        public void onError(FacebookException exception) {
            mIViewLogin.isShowProgress(false);
            Log.e(TAG, "onError: ", exception);
            mIViewLogin.onError(mContext.getString(R.string.somethingWrong));
        }
    };

    @Override
    public void onFacebookLogin(final Activity activity, CallbackManager callbackManager) {
        mIViewLogin.isShowProgress(true);

        LoginManager.getInstance().registerCallback(callbackManager,
                mCallbackLoginResultFacebook);

        LoginManager.getInstance().logInWithReadPermissions(activity, Arrays.asList("public_profile", "email"));
    }
    //    --------- FaceBook END ----------


    //    --------- Twitter START ----------
    private Callback<User> mCallbackTwitterUser = new Callback<User>() {
        @Override
        public void success(Result<User> userResult) {
            String email = userResult.data.email;
            String name = userResult.data.name;

            finishLogin(name, email);
        }

        public void failure(TwitterException exception) {
            mIViewLogin.isShowProgress(false);
            //Do something on failure
            mIViewLogin.onError(mContext.getString(R.string.somethingWrong));
        }
    };

    private Callback<TwitterSession> mCallbackTwitterSession = new Callback<TwitterSession>() {
        @Override
        public void success(final Result<TwitterSession> result) {
            mIViewLogin.isShowProgress(false);
            TwitterApiClient twitterApiClient = TwitterCore.getInstance().getApiClient();
            AccountService statusesService = twitterApiClient.getAccountService();
            Call<User> call = statusesService.verifyCredentials(true, true, true);
            call.enqueue(mCallbackTwitterUser);
        }

        @Override
        public void failure(TwitterException exception) {
            mIViewLogin.isShowProgress(false);
            mIViewLogin.onError(mContext.getString(R.string.somethingWrong));
        }
    };

    @Override
    public TwitterAuthClient onTwitterLogin(Activity activity) {
        mIViewLogin.isShowProgress(true);
        TwitterAuthClient mTwitterAuthClient = new TwitterAuthClient();

        mTwitterAuthClient.authorize(activity, mCallbackTwitterSession);
        return mTwitterAuthClient;
    }
    //    ---------- Twitter END -----------

    // -------------- Google START ------------------
    @Override
    public void onGoogleLogin() {
        mIViewLogin.isShowProgress(true);

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        GoogleApiClient mGoogleApiClient = new GoogleApiClient.Builder(mContext)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        mIViewLogin.googleStartActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    public void handleSignInResult(GoogleSignInResult result) {
        mIViewLogin.isShowProgress(false);
        if (result.isSuccess()) {
            GoogleSignInAccount account = result.getSignInAccount();

            String name = account.getDisplayName();
            String email = account.getEmail();

            finishLogin(name, email);
            Log.d(TAG, "handleSignInResult Google: " + name + " " + email);
        } else {
            mIViewLogin.onError(mContext.getString(R.string.somethingWrong));
            Log.e(TAG, "handleSignInResult: " + result.getStatus().getStatusMessage() + " with Code= " + result.getStatus().getStatusCode());
        }
    }
    // -------------- Google END --------------------

    private void finishLogin(String name, String email) {
        ModelLogin loginModel = new ModelLogin(name, email);
        AppPreferences.writeModelLoginToSP(mContext, loginModel);
        mIViewLogin.onLoginSuccess();
    }
}
