package com.linkdev.task1.login;

import android.app.Activity;
import android.content.Intent;

import com.facebook.CallbackManager;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;

/**
 * Created by Mohammed.AbdElAzeem on 12/12/2017.
 */

public interface IContractLogin {
    public interface IViewLogin {
        void isShowProgress(Boolean isShow);
        void onLoginSuccess();
        void onError(String error);

        void googleStartActivityForResult(Intent signInIntent, int rcSignIn);
    }

    public interface IPresenterLogin {

        void onFacebookLogin(Activity activity, CallbackManager callbackManager);

        TwitterAuthClient onTwitterLogin(Activity activity);

        // -------------- Google START ------------------
        void onGoogleLogin();

        void handleSignInResult(GoogleSignInResult result);
    }
}
