package com.linkdev.task1.news_details;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;

import com.linkdev.task1.R;
import com.linkdev.task1.common.base.BaseActivity;
import com.linkdev.task1.common.helpers.Constants;

public class ActivityNewsDetails extends BaseActivity {
    private static final String EXTRA_ARTICLE_ID = "Article-ID";
    private String TAG = ActivityNewsDetails.class.getSimpleName() + Constants.TAG_UNIVERSAL_LOG;

    private String mArticleId;

    public static void init(Context context, String articleId) {
        Toast.makeText(context, articleId, Toast.LENGTH_LONG).show();
        context.startActivity(new Intent(context, ActivityNewsDetails.class).putExtra(EXTRA_ARTICLE_ID, articleId));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_details);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        initializeViews();

        mArticleId = getIntent().getStringExtra(EXTRA_ARTICLE_ID);

        // COMPLETED: start the fragment
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fl_news_details, FragmentNewsDetails.newInstance(mArticleId))
                .commit();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void initializeViews() {
    }

    @Override
    protected void setListeners() {
    }
}
