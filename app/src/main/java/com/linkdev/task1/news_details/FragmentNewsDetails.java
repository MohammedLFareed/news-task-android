package com.linkdev.task1.news_details;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.linkdev.task1.R;
import com.linkdev.task1.common.base.BaseFragment;
import com.linkdev.task1.common.helpers.Constants;
import com.linkdev.task1.common.helpers.ServicesHelper;
import com.linkdev.task1.common.models.ModelArticleDetails;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link FragmentNewsDetails#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentNewsDetails extends BaseFragment implements INewsDetailsContract.INewsDetailsView {
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_NEWS_ID = "arg-news-details";

    private String TAG = ActivityNewsDetails.class.getSimpleName() + Constants.TAG_UNIVERSAL_LOG;
    private Context mContext;

    @BindView(R.id.img_article)
    ImageView mArticleImageView;
    @BindView(R.id.tv_article_title)
    TextView mTitleTextView;
    @BindView(R.id.tv_article_date)
    TextView mDateTextView;
    @BindView(R.id.tv_article_description)
    TextView mDescriptionTextView;
    @BindView(R.id.tv_article_likes)
    TextView mLikesTextView;
    @BindView(R.id.tv_article_views)
    TextView mViewsTextView;
    @BindView(R.id.progressArticle)
    ProgressBar mProgressArticle;
    @BindView(R.id.nscrollView)
    View mParentView;
    private INewsDetailsContract.INewsDetailsPresenter mIPresenterNewsDetails;
    private String mArticleId;
    private View.OnClickListener OnLikesClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
        }
    };

    public FragmentNewsDetails() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param_news_id Parameter 1.
     * @return A new instance of fragment FragmentNewsDetails.
     */
    public static FragmentNewsDetails newInstance(String param_news_id) {
        FragmentNewsDetails fragment = new FragmentNewsDetails();
        Bundle args = new Bundle();
        args.putString(ARG_NEWS_ID, param_news_id);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mArticleId = getArguments().getString(ARG_NEWS_ID);
        }
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_news_details, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initializeViews(view);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mContext = getActivity();
        mIPresenterNewsDetails = new PresenterNewsDetails(this, mContext);
        mIPresenterNewsDetails.loadNewsDetails(getActivity(), mArticleId);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ServicesHelper.getInstance(getActivity()).getRequestQueue().cancelAll(ServicesHelper.Tag.NEWS_DETAILS);
    }

    @Override
    protected void initializeViews(View v) {
        ButterKnife.bind(this, v);
    }

    @Override
    protected void setListeners() {
        mLikesTextView.setOnClickListener(OnLikesClickListener);
    }

    @Override
    public void isShowProgress(Boolean isShow) {
        if (isShow) {
            mProgressArticle.setVisibility(View.VISIBLE);
            mParentView.setVisibility(View.GONE);
        } else {
            mProgressArticle.setVisibility(View.GONE);
            mParentView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onDataLoaded(ModelArticleDetails modelArticleDetails) {
        String numViews = getString(R.string.string_s_views, modelArticleDetails.getNumofViews());
        String likes = getString(R.string.string_likes_s, modelArticleDetails.getLikes());
        String date = modelArticleDetails.getPostDate();
        String title = modelArticleDetails.getNewsTitle();
        String desc = modelArticleDetails.getItemDescription();
        String image = modelArticleDetails.getImageUrl();

        Picasso.with(getActivity())
                .load(image)
                .into(mArticleImageView);
        mDateTextView.setText(date);
        mDescriptionTextView.setText(desc);
        mLikesTextView.setText(likes);
        mTitleTextView.setText(title);
        mViewsTextView.setText(numViews);

        mDescriptionTextView.setMovementMethod(new ScrollingMovementMethod());
    }

    @Override
    public void onError(String error) {
        Toast.makeText(mContext, error, Toast.LENGTH_LONG).show();
        Log.d(TAG, "onError: " + error);

    }

    @Override
    public void triggerShare(String shareUrl, String articleTitle) {
        Intent share = new Intent(android.content.Intent.ACTION_SEND);
        share.setType("text/plain");
        share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);

        // Add data to the intent, the receiving app will decide
        // what to do with it.
        share.putExtra(Intent.EXTRA_SUBJECT, articleTitle);
        share.putExtra(Intent.EXTRA_TEXT, shareUrl);

        startActivity(Intent.createChooser(share, getString(R.string.share_article)));
    }

    public void updateFragment(String nid) {
        mArticleId = nid;
        mIPresenterNewsDetails.loadNewsDetails(mContext, nid);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_article_details, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_share:
                mIPresenterNewsDetails.onShare(getActivity());
                return true;
        }
        return false;
    }

}
