package com.linkdev.task1.news_details;

import android.content.Context;

import com.linkdev.task1.common.models.ModelArticleDetails;

/**
 * Created by Mohammed.AbdElAzeem on 12/6/2017.
 */

public interface INewsDetailsContract {
    public interface INewsDetailsView {
        void isShowProgress(Boolean isShow);
        void onDataLoaded(ModelArticleDetails modelArticleDetails);
        void onError(String error);

        void triggerShare(String shareUrl, String articleTitle);
    }

    public interface INewsDetailsPresenter {

        void loadNewsDetails(Context context, String nId);

        void onShare(Context context);
    }
}
