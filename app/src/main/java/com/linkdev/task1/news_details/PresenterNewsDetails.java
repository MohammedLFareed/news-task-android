package com.linkdev.task1.news_details;

import android.content.Context;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.linkdev.task1.R;
import com.linkdev.task1.common.base.BasePresenter;
import com.linkdev.task1.common.helpers.Constants;
import com.linkdev.task1.common.helpers.MyGson;
import com.linkdev.task1.common.helpers.ServicesHelper;
import com.linkdev.task1.common.helpers.VolleyErrorHandler;
import com.linkdev.task1.common.models.dto.ArticleResponse;

import org.json.JSONObject;

/**
 * Created by Mohammed.AbdElAzeem on 12/4/2017.
 */

public class PresenterNewsDetails extends BasePresenter implements INewsDetailsContract.INewsDetailsPresenter {
    private static final String TAG = PresenterNewsDetails.class.getSimpleName() + Constants.TAG_UNIVERSAL_LOG;
    private INewsDetailsContract.INewsDetailsView mINewsDetailsView;
    private Context mContext;
    private ArticleResponse mModelArticleDetails;

    private Response.Listener<JSONObject> mNewsDetailsSuccessListener = new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject response) {
            mINewsDetailsView.isShowProgress(false);
            if (response != null) {
                mModelArticleDetails = MyGson.getmGson().fromJson(response.toString(), ArticleResponse.class);
                mINewsDetailsView.onDataLoaded(mModelArticleDetails.getNewsItem());
            } else {
                mINewsDetailsView.onError(mContext.getString(R.string.somethingWrong));
            }
        }
    };
    private Response.ErrorListener mNewsDetailsErrorListener = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            mINewsDetailsView.isShowProgress(false);
            mINewsDetailsView.onError(VolleyErrorHandler.getErrorMessage(mContext, error));
        }
    };

    PresenterNewsDetails(INewsDetailsContract.INewsDetailsView newsDetailsView, Context context) {
        mINewsDetailsView = newsDetailsView;
        mContext = context;
    }

    @Override
    public void loadNewsDetails(Context context, String nId) {
        mINewsDetailsView.isShowProgress(true);
        ServicesHelper.getInstance(context)
                .getNewsDetails(nId, mNewsDetailsSuccessListener, mNewsDetailsErrorListener);
    }

    @Override
    public void onShare(Context context) {
        if (mModelArticleDetails == null) {
            mINewsDetailsView.onError(mContext.getString(R.string.somethingWrong));
            return;
        }
        String shareUrl = mModelArticleDetails.getNewsItem().getShareURL();
        String articleTitle = mModelArticleDetails.getNewsItem().getNewsTitle();
        mINewsDetailsView.triggerShare(shareUrl, articleTitle);
    }
}
