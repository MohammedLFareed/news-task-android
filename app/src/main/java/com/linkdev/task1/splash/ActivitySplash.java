package com.linkdev.task1.splash;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;

import com.linkdev.task1.common.helpers.AppPreferences;
import com.linkdev.task1.common.helpers.LocalizationHelper;
import com.linkdev.task1.drawer.ActivityMain;
import com.linkdev.task1.login.ActivityLogin;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import static com.linkdev.task1.common.helpers.Constants.EXTRA_CHANGE_LOCALE;

/**
 * Created by Mohammed.AbdElAzeem on 12/11/2017.
 */

public class ActivitySplash extends AppCompatActivity {

    public static void restart_ChangeLocale(Context context, Boolean isChangeLocale) {
        context.startActivity(new Intent(context, ActivitySplash.class).putExtra(EXTRA_CHANGE_LOCALE, isChangeLocale));
    }

    public static void init(Context context) {
        context.startActivity(new Intent(context, ActivitySplash.class));
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        GetKeyHash();
        super.onCreate(savedInstanceState);
        // Language setup
        if (getIntent().hasExtra(EXTRA_CHANGE_LOCALE)) {
            LocalizationHelper.changeAppLanguage(this, true); // switch the current lang
        } else {
            LocalizationHelper.changeAppLanguage(this, false); // the first time
        }

        //: login check
        if (AppPreferences.readModelLoginSP(this) == null) // not logged in
            ActivityLogin.init(this);
        else
            ActivityMain.init(this);
        finish();
    }

    private void GetKeyHash() {
        PackageInfo info;
        try {
            info = getPackageManager().getPackageInfo(getPackageName(), PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md;
                md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String keyhash = new String(Base64.encode(md.digest(), 0));
                // String something = new String(Base64.encodeBytes(md.digest()));
                Log.e("keyhash", "keyhash= " + keyhash);
                System.out.println("keyhash= " + keyhash);
            }
        } catch (PackageManager.NameNotFoundException e1) {
            Log.e("name not found", e1.toString());
        } catch (NoSuchAlgorithmException e) {
            Log.e("no such an algorithm", e.toString());
        } catch (Exception e) {
            Log.e("exception", e.toString());
        }

    }
}
