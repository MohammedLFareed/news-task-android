package com.linkdev.task1.news;

import android.content.Context;

/**
 * Created by Mohammed.AbdElAzeem on 12/4/2017.
 */

public interface IContractNews {

    public interface INewsView {
        void isShowProgress(boolean isShowProgress);

        void onNewsLoaded();

        void onError(String volleyError);

        void onNewsSelected(String nid, Boolean isSelectFirstItem);
    }

    public interface INewsPresenter {
        int getNewsItemCount();

        void loadNews(Context context);

        void onNewsBindViewHolder(AdapterNews.INewsViewHolder iNewsViewHolder, int position);

        void onNewsItemClickListener(int adapterPosition);
    }
}
