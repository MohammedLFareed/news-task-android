package com.linkdev.task1.news;

import android.content.Context;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.linkdev.task1.common.base.BasePresenter;
import com.linkdev.task1.common.helpers.Constants;
import com.linkdev.task1.common.helpers.MyGson;
import com.linkdev.task1.common.helpers.ServicesHelper;
import com.linkdev.task1.common.helpers.VolleyErrorHandler;
import com.linkdev.task1.common.models.ModelNews;
import com.linkdev.task1.common.models.dto.NewsResponse;

import org.json.JSONObject;

import java.util.List;

/**
 * Created by Mohammed.AbdElAzeem on 12/4/2017.
 */

public class PresenterNews extends BasePresenter implements com.linkdev.task1.news.IContractNews.INewsPresenter {
    private static final String TAG = PresenterNews.class.getSimpleName() + Constants.TAG_UNIVERSAL_LOG;
    private final Context mContext;
    private IContractNews.INewsView mINewsView;

    private Response.Listener<JSONObject> newsSuccessListener = new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject response) {
            mINewsView.isShowProgress(false);
            if (response != null) {
                NewsResponse newsResponse = MyGson.getmGson().fromJson(response.toString(), NewsResponse.class);
                if (newsResponse != null) {
                    mINewsView.onNewsLoaded();
                    mNewsList = newsResponse.getModelNews();
                    mINewsView.onNewsSelected(mNewsList.get(0).getNid(), true);
                } else
                    mINewsView.onError(null);
            } else {
                mINewsView.onError(null);
            }
        }
    };
    private Response.ErrorListener newsErrorListener = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            mINewsView.isShowProgress(false);
            VolleyErrorHandler.getErrorMessage(mContext, error);
            mINewsView.onError(VolleyErrorHandler.getErrorMessage(mContext, error));
        }
    };

    private List<ModelNews> mNewsList;

    public PresenterNews(com.linkdev.task1.news.IContractNews.INewsView iNewsView, Context context) {
        this.mINewsView = iNewsView;
        mContext = context;
    }

    @Override
    public int getNewsItemCount() {
        return mNewsList.size();
    }

    @Override
    public void loadNews(Context context) {
        mINewsView.isShowProgress(true);

        ServicesHelper.getInstance(context).getNews(newsSuccessListener, newsErrorListener);
    }

    @Override
    public void onNewsBindViewHolder(AdapterNews.INewsViewHolder iNewsViewHolder, int position) {
        iNewsViewHolder.onBind(mNewsList.get(position));
    }

    @Override
    public void onNewsItemClickListener(int adapterPosition) {
        mINewsView.onNewsSelected(mNewsList.get(adapterPosition).getNid(), false);
    }
}
