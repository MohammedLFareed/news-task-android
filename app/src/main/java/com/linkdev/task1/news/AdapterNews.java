package com.linkdev.task1.news;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.linkdev.task1.R;
import com.linkdev.task1.common.models.ModelNews;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.linkdev.task1.common.helpers.Constants.NEWS_TYPE_ARTICLE;

/**
 * Created by Mohammed.AbdElAzeem on 12/4/2017.
 */

public class AdapterNews extends RecyclerView.Adapter<AdapterNews.NewsViewHolder> {
    private IContractNews.INewsPresenter mINewsPresenter;

    AdapterNews(IContractNews.INewsPresenter iNewsPresenter) {
        mINewsPresenter = iNewsPresenter;
    }

    @Override
    public NewsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new NewsViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_news_row, parent, false));
    }

    @Override
    public void onBindViewHolder(NewsViewHolder holder, int position) {
        mINewsPresenter.onNewsBindViewHolder(holder, position);
    }

    @Override
    public int getItemCount() {
        return mINewsPresenter.getNewsItemCount();
    }

    //----------- ViewHolder -----------
    class NewsViewHolder extends RecyclerView.ViewHolder implements INewsViewHolder {
        private boolean mIsArabic = false;
        @BindView(R.id.img_news_type)
        ImageView typeImageView;
        @BindView(R.id.img_news)
        ImageView articleImageView;
        @BindView(R.id.tv_news_title)
        TextView newsTitleTextView;
        @BindView(R.id.tv_news_date)
        TextView newsDateTextView;
        @BindView(R.id.tv_news_likes)
        TextView newsLikesTextView;
        @BindView(R.id.tv_news_views)
        TextView newsViewsTextView;

        NewsViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            if (itemView.findViewById(R.id.item_news_row_ar) != null)
                mIsArabic = true;
            else
                mIsArabic = false;
        }

        @Override
        public void onBind(ModelNews modelNews) {
            String likes = itemView.getContext().getString(R.string.string_likes_s, modelNews.getLikes());
            String views = itemView.getContext().getString(R.string.string_s_views, modelNews.getNumOfViews());
            newsTitleTextView.setText(modelNews.getNewsTitle());
            newsDateTextView.setText(modelNews.getPostDate());
            newsLikesTextView.setText(likes);
            newsViewsTextView.setText(views);
            Picasso.with(itemView.getContext())
                    .load(modelNews.getImageUrl())
                    .placeholder(R.drawable.news_image_placeholder)
                    .into(articleImageView);
            if (mIsArabic) {
                if (modelNews.getNewsType().equals(NEWS_TYPE_ARTICLE))
                    typeImageView.setImageResource(R.drawable.article_label_ar);
                else
                    typeImageView.setImageResource(R.drawable.video_label_ar);
            } else {
                if (modelNews.getNewsType().equals(NEWS_TYPE_ARTICLE))
                    typeImageView.setImageResource(R.drawable.article_label);
                else
                    typeImageView.setImageResource(R.drawable.video_label);
            }

            itemView.setTag(modelNews.getNid());
            itemView.setOnClickListener(onItemClickListener);
        }

        private View.OnClickListener onItemClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mINewsPresenter.onNewsItemClickListener(getAdapterPosition());
            }
        };

    }

    interface INewsViewHolder {
        void onBind(ModelNews modelNews);
    }
}
