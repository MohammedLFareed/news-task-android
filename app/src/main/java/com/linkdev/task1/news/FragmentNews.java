package com.linkdev.task1.news;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.linkdev.task1.R;
import com.linkdev.task1.common.base.BaseFragment;
import com.linkdev.task1.common.helpers.Constants;
import com.linkdev.task1.common.helpers.ServicesHelper;
import com.linkdev.task1.news_details.ActivityNewsDetails;
import com.linkdev.task1.news_details.FragmentNewsDetails;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link FragmentNews#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentNews extends BaseFragment implements IContractNews.INewsView {

    private static final String TAG = FragmentNews.class.getSimpleName() + Constants.TAG_UNIVERSAL_LOG;
    private static final String FRAGMENT_DETAILS_TAG = "fragment-details-tag";
    private Context mContext;
    private IContractNews.INewsPresenter mINewsPresenter;
    @BindView(R.id.rv_news)
    RecyclerView mNewsRecyclerView;
    @BindView(R.id.srl_news)
    SwipeRefreshLayout mNewsSwipeRefreshLayout;
    private boolean mIsTablet = false;

    public FragmentNews() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment FragmentNews.
     */
    public static FragmentNews newInstance() {
        FragmentNews fragment = new FragmentNews();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_news, container, false);
        initializeViews(view);

        if (view.findViewById(R.id.fl_main_details) != null) { // tabletMode
            mIsTablet = true;
        } else
            mIsTablet = false;
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mContext = getActivity();
        mINewsPresenter = new PresenterNews(this, mContext);
        mINewsPresenter.loadNews(mContext);
    }

    @Override
    protected void initializeViews(View view) {
        ButterKnife.bind(this, view);
        mNewsRecyclerView.setHasFixedSize(true);
        mNewsRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        mNewsSwipeRefreshLayout.setColorSchemeColors(
                getResources().getColor(R.color.colorAccent));
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ServicesHelper.getInstance(mContext).getRequestQueue().cancelAll(ServicesHelper.Tag.NEWS);
    }

    @Override
    protected void setListeners() {
        mNewsSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mINewsPresenter.loadNews(mContext);
            }
        });
    }

    @Override
    public void isShowProgress(boolean isShowProgress) {
        if (isShowProgress)
            mNewsSwipeRefreshLayout.setRefreshing(true);
        else
            mNewsSwipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onNewsLoaded() {
        mNewsRecyclerView.setAdapter(new AdapterNews(mINewsPresenter));
    }

    @Override
    public void onError(String error) {
        Toast.makeText(mContext, error, Toast.LENGTH_SHORT).show();
    }

    /**
     *
     * @param nid article id
     * @param isSelectFirstItem in case it's the first launch in the tablet mode we should select the first item.
     */
    @Override
    public void onNewsSelected(String nid, Boolean isSelectFirstItem) {
        if (mIsTablet) {
            if (getChildFragmentManager().findFragmentByTag(FRAGMENT_DETAILS_TAG) == null) {
                getChildFragmentManager().beginTransaction()
                        .replace(R.id.fl_main_details, FragmentNewsDetails.newInstance(nid), FRAGMENT_DETAILS_TAG)
                        .addToBackStack("Article-Details_BackStack")
                        .commitAllowingStateLoss();
            } else {
                FragmentNewsDetails fragmentNewsDetails =
                        (FragmentNewsDetails) getChildFragmentManager().findFragmentByTag(FRAGMENT_DETAILS_TAG);
                fragmentNewsDetails.updateFragment(nid);
            }
        } else if (!isSelectFirstItem) {
            ActivityNewsDetails.init(mContext, nid);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_news, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_filter) {
            Toast.makeText(mContext, item.getTitle(), Toast.LENGTH_SHORT).show();
            return true;
        }
        return false;
    }

}
