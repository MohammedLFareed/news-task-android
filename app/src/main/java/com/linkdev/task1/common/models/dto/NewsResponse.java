package com.linkdev.task1.common.models.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.linkdev.task1.common.models.ModelNews;

import java.util.List;

/**
 * Created by Sherif.ElNady on 2/26/2017.
 */

public class NewsResponse {
    @SerializedName("News")
    @Expose
    private List<ModelNews> modelNews = null;

    public List<ModelNews> getModelNews() {
        return modelNews;
    }

    public void setModelNews(List<ModelNews> modelNews) {
        this.modelNews = modelNews;
    }
}
