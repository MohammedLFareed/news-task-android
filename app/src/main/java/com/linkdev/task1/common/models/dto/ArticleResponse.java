package com.linkdev.task1.common.models.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.linkdev.task1.common.models.ModelArticleDetails;

/**
 * news-task-android Created by Mohammed Fareed on 12/9/2017.
 */

public class ArticleResponse {

    @SerializedName("newsItem")
    @Expose
    private ModelArticleDetails newsItem;

    public ModelArticleDetails getNewsItem() {
        return newsItem;
    }

    public void setNewsItem(ModelArticleDetails newsItem) {
        this.newsItem = newsItem;
    }
}
