package com.linkdev.task1.innovation_maps;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.location.LocationManager;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.util.Log;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.linkdev.task1.R;
import com.linkdev.task1.common.helpers.Constants;
import com.linkdev.task1.common.helpers.LocationHelper;
import com.linkdev.task1.common.helpers.PermissionsHelper;

import static com.linkdev.task1.common.helpers.LocationHelper.isLocationServiceDialogShown;
import static com.linkdev.task1.common.helpers.LocationHelper.isLocationServiceOn;
import static com.linkdev.task1.common.helpers.PermissionsHelper.isPermissionGranted;

/**
 * Created by Mohammed.AbdElAzeem on 12/21/2017.
 */

public class PresenterInnovationMap implements IContractInnovationMap.IPresenter {
    private static final String TAG = PresenterInnovationMap.class.getSimpleName() + Constants.TAG_UNIVERSAL_LOG;
    private final FusedLocationProviderClient mFusedLocationClient;
    private IContractInnovationMap.IView mIView;
    private Context mContext;
    private LocationRequest mLocationRequest;
    private GoogleMap mGoogleMap;
    private LocationCallback mLocationCallback;

    private BroadcastReceiver mLocationServiceStateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            LocationHelper.checkLocationService(context, mEnableLocationSuccessListener);
        }
    };
    private DialogInterface.OnClickListener mEnableLocationSuccessListener =
            new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    mContext.startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    isLocationServiceDialogShown = false;
                }
            };

    PresenterInnovationMap(IContractInnovationMap.IView iView, Fragment fragment) {
        this.mIView = iView;
        mContext = fragment.getContext();

        // setup FusedLocationClient
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(mContext);

        if (!isPermissionGranted(mContext, android.Manifest.permission.ACCESS_FINE_LOCATION)) // check permission granted
            PermissionsHelper.requestPermissionFragment(mContext, fragment, Manifest.permission.ACCESS_FINE_LOCATION);
        else
            onPermissionGrated();
    }

    private void addMarkerToMapAtLocation(LatLng mCurLocationLatlng) {
        mGoogleMap.addMarker(new MarkerOptions().position(mCurLocationLatlng)
                .title(mContext.getString(R.string.current_location)));
        CameraPosition newCamPos = new CameraPosition(mCurLocationLatlng,
                15.5f,
                mGoogleMap.getCameraPosition().tilt,
                mGoogleMap.getCameraPosition().bearing);
        mGoogleMap.animateCamera(CameraUpdateFactory.newCameraPosition(newCamPos), 2000, null);
    }

    @SuppressLint("MissingPermission")
    private void startLocationUpdates() {
        // setup mLocationRequest
        mLocationRequest = new LocationRequest();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);

        // Location CallBack
        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                for (Location location : locationResult.getLocations()) {
                    LatLng mCurLocationLatlng = new LatLng(location.getLatitude(), location.getLongitude());
                    addMarkerToMapAtLocation(mCurLocationLatlng);
                    stopLocationUpdates();
                }
            }
        };

        mFusedLocationClient.requestLocationUpdates(mLocationRequest,
                mLocationCallback,
                null /* Looper */);
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;
        if (isPermissionGranted(mContext, android.Manifest.permission.ACCESS_FINE_LOCATION)) {
            mGoogleMap.setMyLocationEnabled(true);
        }
    }

    @Override
    public void onPermissionGrated() {
        if (!LocationHelper.isLocationServiceOn(mContext)) { // check if location or GPS is turned on
            LocationHelper.checkLocationService(mContext, mEnableLocationSuccessListener);
        } else {
            startLocationUpdates();
        }
    }

    @Override
    public void onResume() {
        if (mFusedLocationClient != null && mGoogleMap != null) {
            Log.d(TAG, "onResume: location and map != null");
            if (isPermissionGranted(mContext, android.Manifest.permission.ACCESS_FINE_LOCATION)) {
                if (isLocationServiceOn(mContext))
                    startLocationUpdates();
            }
        }
        try {
            mContext.registerReceiver(mLocationServiceStateReceiver, new IntentFilter(LocationManager.PROVIDERS_CHANGED_ACTION));
        } catch (Exception e) {
            Log.e(TAG, "onResume: ", e);
        }
    }

    @Override
    public void onDestroy() {
        stopLocationUpdates();
        try {
            mContext.unregisterReceiver(mLocationServiceStateReceiver);
        } catch (Exception e) {
            Log.e(TAG, "onDestroy: ", e);
        }
    }

    private void stopLocationUpdates() {
        if (mLocationRequest != null)
            mFusedLocationClient.removeLocationUpdates(mLocationCallback);
    }

}
