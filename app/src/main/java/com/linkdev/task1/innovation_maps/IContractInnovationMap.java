package com.linkdev.task1.innovation_maps;

import com.google.android.gms.maps.GoogleMap;

/**
 * Created by Mohammed.AbdElAzeem on 12/10/2017.
 */

public interface IContractInnovationMap {
    public interface IPresenter {
        void onMapReady(GoogleMap googleMap);

        void onPermissionGrated();

        void onResume();

        void onDestroy();

    }

    public interface IView {
    }
}
