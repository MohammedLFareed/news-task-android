package com.linkdev.task1.innovation_maps;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.linkdev.task1.R;
import com.linkdev.task1.common.base.BaseFragment;
import com.linkdev.task1.common.helpers.Constants;
import com.linkdev.task1.common.helpers.LocationHelper;

import static com.linkdev.task1.common.helpers.LocationHelper.isLocationServiceDialogShown;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link FragmentInnovationMap#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentInnovationMap extends BaseFragment implements IContractInnovationMap.IView, OnMapReadyCallback {
    private final String TAG = FragmentInnovationMap.class.getSimpleName() + Constants.TAG_UNIVERSAL_LOG;

    private PresenterInnovationMap mIPresenter;

    public FragmentInnovationMap() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment FragmentInnovationMap.
     */
    public static FragmentInnovationMap newInstance() {
        FragmentInnovationMap fragment = new FragmentInnovationMap();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_innovation_map, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        ((SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.map)).getMapAsync(this);

        mIPresenter = new PresenterInnovationMap(this, this);
    }

    @Override
    public void onResume() {
        super.onResume();
        mIPresenter.onResume();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mIPresenter.onMapReady(googleMap);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
//        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (permissions[0].equals(Manifest.permission.ACCESS_FINE_LOCATION)
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            mIPresenter.onPermissionGrated();
        }
    }

    @Override
    protected void initializeViews(View v) {
    }

    @Override
    protected void setListeners() {
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mIPresenter.onDestroy();
    }
}

